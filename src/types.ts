type GlobalProps = {
  pageContext: {
    locale: string;
    localeResources?: {
      translation?: any
    }
  }
  path: string;
}

declare module '*.svg' {
  const content: any;
  export default content;
}

declare module 'styled-components-grid' {
  export default Grid;
}
