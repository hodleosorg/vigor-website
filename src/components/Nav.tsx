import React, {useCallback, useRef, useState} from "react";
import styled from "styled-components";
import {media} from "../utils/breakpoints";
import LanguageDropdown from "./LanguageDropdown";
import {BlueButton} from "./shared";
import LocalizedLink from "./LocalizedLink";
import {useTranslation} from "react-i18next";
import { useOnClickOutside } from '../utils/hooks';

const NavWrap = styled.div<any>`

`;

const BurgerButtonWrap = styled.div<any>`
  z-index: 1;
  width: 42px;
  height: 40px;
  position: relative;
  transform: rotate(0deg);
  transition: .25s ease-in-out;
  cursor: pointer;
  display: none;

  ${media.lessThan('sm')} {
    display: block;
    
    ${props => props.isOpen && `
      width: 30px;
      margin-right: 8px;
    `}
  }
  
  span {
    display: block;
    position: absolute;
    height: 6px;
    width: 100%;
    background: ${props => props.theme.colors.white};
    border-radius: 9px;
    opacity: 1;
    left: 0;
    transform: rotate(0deg);
    transition: .25s ease-in-out;
    
    &:nth-child(1) {
      top: 0px;
    }
    
    &:nth-child(2),
    &:nth-child(3) {
      top: 14px;
    }
    
    &:nth-child(4) {
      top: 28px;
    }
    
    ${props => props.isOpen && `
      &:nth-child(1) {
        top: 18px;
        width: 0%;
        left: 50%;
      }
      &:nth-child(2) {
        transform: rotate(45deg);
      }
      
      &:nth-child(3) {
        transform: rotate(-45deg);
      }
      
      &:nth-child(4) {
        top: 18px;
        width: 0%;
        left: 50%;
      }
    `}
  }
`;

const StyledLocalizedLink = styled(LocalizedLink)`
  font-size: 15px;
  line-height: 19px;
  margin-right: 48px;
  
  ${media.lessThan('md')} {
    margin-right: 26px;
  }
  
  ${media.lessThan('sm')} {
    line-height: 24px;
  }
`;

const NavBlock = styled.nav<any>`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex: 0 0 auto;
  transition: .25s ease-in-out;
   
  ${media.lessThan('sm')} {
    position: absolute;
    background: ${props => props.theme.colors.bgLighter};
    padding: 16px;
    padding-top: 54px;
    top: 0;
    flex-direction: column;
    align-items: flex-start;
    width: 0%;
    opacity: 0;
    right: 133px;
    box-shadow: 0 8px 6px -6px black;
    padding-top: 80px;
    visibility: hidden;

    ${props => props.isOpen && `
      opacity: 1;
      width: 100%;
      right: 0;
      visibility: visible;
    `}
  }
`;

const Nav: React.FC<GlobalProps> = props => {
  const [ open, setOpen ] = useState(false);
  const { t } = useTranslation();
  const ref = useRef();
  const handleClickOutside = useCallback(() => setOpen(false), [setOpen]);
  useOnClickOutside(ref, handleClickOutside);

  return (
    <NavWrap ref={ref}>
      <BurgerButtonWrap onClick={() => setOpen(!open)} isOpen={open}>
        <span/>
        <span/>
        <span/>
        <span/>
      </BurgerButtonWrap>
      <NavBlock isOpen={open}>
        <StyledLocalizedLink to="/products">{t(`products`)}</StyledLocalizedLink>
        <StyledLocalizedLink to="/learn">{t(`learn`)}</StyledLocalizedLink>
        <StyledLocalizedLink to="/community">{t(`community`)}</StyledLocalizedLink>
        <StyledLocalizedLink to="/faq">{t(`faqShort`)}</StyledLocalizedLink>
        <LanguageDropdown path={props.path} />
        <BlueButton
          as="a"
          margin="0 0 0 32px"
          href="https://newdex.io/trade/vig111111111-vig-eos"
          target="_blank"
          rel="noopener noreferrer nofollow"
        >
          {t(`buyVig`)}
        </BlueButton>
      </NavBlock>
    </NavWrap>
  )
};

export default Nav;
