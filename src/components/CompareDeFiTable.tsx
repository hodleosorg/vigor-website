import React, { useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import { media } from "../utils/breakpoints";
import VigorLogo from "./VigorLogo";
import Jumbotron from "./Jumbotron";

const CompareDeFiWrap = styled.div`

`;

const TableWrap = styled.div`
  margin-top: 50px;
  font-weight: 400;
`;

const TableColumn = styled.div`
  font-weight: 500;
  display: flex;
  flex-direction: column;
  flex-basis: 100%;
  flex: 1;
  
  margin-left: 2px;
  padding-top: 18px;
  padding-bottom: 18px;
  
  background-color: ${({ theme }) => theme.colors.bgLighter};
  
  ${media.lessThan('xs-max')} {
    order: 2;
  }
`;

const TableColumnHeader = styled(TableColumn)`
  margin-left: 0;
  font-size: 15px;
  color: #BCBDC1;
  line-height: 19px;
  text-align: initial;
  background-color: ${({ theme }) => theme.colors.bgLight};
  
  padding-left: 44px;
  
  ${media.lessThan('xs-max')} {
    order: 1;
    font-size: 14px;
    padding-left: 17px;
  }
`;

const TableColumnPrimary = styled(TableColumn)`  
  background-color: ${({ theme }) => theme.colors.primary};
  font-size: 16px;
  align-items: center; 
  
  ${media.lessThan('xs-max')} {
    order: 1;
    font-size: 14px;
  }
`;

const TableColumnSecondary = styled(TableColumn)`
  font-size: 16px;
  align-items: center;
  
  ${media.lessThan('xs-max')} {
    font-size: 14px;
    
    &.hidden {
      display: none;
    }
  }
`;

const TableRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  
  :not(:last-of-type):not(:first-of-type) {
    border-bottom: 1px solid ${({ theme }) => theme.colors.bg}
  }

  &:not(.last) {
    & > ${TableColumnPrimary},
    & > ${TableColumnSecondary} {
      justify-content: center;
    }
  }
  
  &.first {
    & > ${TableColumnHeader} {
      border-radius: 4px 0 0 0;
      margin-top: 52px;
      padding-top: 24px;
    }
    
    & > ${TableColumnPrimary} {
      border-radius: 4px 4px 0 0;
      padding-top: 35px;
    }
    
    & > ${TableColumnSecondary} {
      border-radius: 4px 4px 0 0;
      margin-top: 17px;
    }
  }
  
  &.last {
    & > ${TableColumnHeader} {
      border-radius: 0 0 0 4px;
      margin-bottom: 52px;
      padding-bottom: 60px;
    }
    
    & > ${TableColumnPrimary} {
      border-radius: 0 0 4px 4px;
    }
    
    & > ${TableColumnSecondary} {
      border-radius: 0 0 4px 4px;
      margin-bottom: 17px;
    }
  }
`;

const TableFooter = styled(TableRow)`
  display: none;
  justify-content: space-around;
  
  ${media.lessThan('xs-max')} {
    display: flex;
  }
`;

const PagingButton = styled.button`
  border-radius: 100%;
  background-color: ${({ theme }) => theme.colors.light};
  width: 10px;
  height: 10px;
  
  &:last-of-type {
    margin-left: 10px;
    margin-right: 15px;
  }
  
  &:disabled {
    cursor: initial;
    background-color: ${({ theme }) => theme.colors.primary};
  }
`;

const ShieldCheckSvgIcon = () =>
  <svg width="26px" height="32px" viewBox="0 0 26 32" xmlns="http://www.w3.org/2000/svg" >
    <g id="shield-check">
      <path d="M3.2,7.0176 L3.2,12.8 C3.20045642,16.9608825 4.99673321,20.9191067 8.128,23.6592 L12.8,27.7472 L17.472,23.6592 C20.6034387,20.9192344 22.3997529,16.9609277 22.4,12.8 L22.4,7.0176 L12.8,3.4176 L3.2,7.0176 Z M1.0384,4.4096 L12.8,0 L24.5616,4.4112 C25.1859833,4.6452498 25.6,5.24199135 25.6,5.9088 L25.6,12.8 C25.6,17.8832 23.4048,22.72 19.5792,26.0688 L12.8,32 L6.0208,26.0688 C2.19455227,22.720884 1.22335475e-15,17.884163 1.22335475e-15,12.8 L1.22335475e-15,5.9088 C1.22335475e-15,5.24199135 0.414016685,4.6452498 1.0384,4.4112 L1.0384,4.4096 Z M11.1024,16.4288 L16.76,10.7712 C17.3878062,10.1648445 18.3857356,10.1735162 19.0029097,10.7906903 C19.6200838,11.4078644 19.6287555,12.4057938 19.0224,13.0336 L12.2336,19.824 C11.6088002,20.4486111 10.5959998,20.4486111 9.9712,19.824 L6.5776,16.4304 C5.97124446,15.8025938 5.9799162,14.8046644 6.59709028,14.1874903 C7.21426436,13.5703162 8.21219378,13.5616445 8.84,14.168 L11.1024,16.4304 L11.1024,16.4288 Z"/>
    </g>
  </svg>
;

const EquilibriumLogo = () =>
  <svg width="122" height="41" xmlns="http://www.w3.org/2000/svg"  style={{ width: '75%' }} viewBox="0 0 123 50">
    <g fill="none" fillRule="evenodd">
      <path d="M114.183 33.995v-2.688l1.917 2.688h3.305l-2.804-3.653 2.393-2.985h-3.1l-1.711 2.341V24.99h-2.702v9.005h2.702zm-6.08 0v-3.743c.36-.386.9-.579 1.621-.579.27 0 .54.039.81.102v-2.547c-.09-.025-.219-.038-.36-.038-.463 0-.888.142-1.287.411-.398.271-.656.541-.784.811v-1.055h-2.702v6.638h2.702zm-7.371.167c1.094 0 2.007-.334 2.714-.99.72-.656 1.081-1.492 1.081-2.495 0-1.004-.361-1.84-1.081-2.496-.707-.657-1.62-.991-2.714-.991-1.093 0-1.994.334-2.714.991-.708.656-1.068 1.492-1.068 2.496 0 1.003.36 1.839 1.068 2.495.72.656 1.621.99 2.714.99zm-.823-2.624c-.219-.231-.322-.515-.322-.861 0-.348.103-.631.322-.85.423-.463 1.222-.463 1.646 0 .219.219.322.502.322.85 0 .346-.103.63-.322.861-.424.45-1.223.45-1.646 0zm-9.558 2.509l.951-3.101.991 3.101h2.289l2.149-6.69H94.12l-.837 3.101-.887-3.126h-2.058l-.862 3.139-.875-3.114H85.99l2.097 6.69h2.264zm-8.906-4.078c.09-.618.412-.965.913-.965.463 0 .836.36.848.965h-1.761zm1.183 4.193c1.235 0 2.149-.219 2.74-.643v-1.981c-.63.335-1.35.502-2.135.502-.784 0-1.518-.258-1.737-.785h4.22v-.617c0-1.055-.309-1.891-.928-2.509-.616-.63-1.388-.939-2.301-.939-1.068 0-1.93.334-2.586 1.003-.656.669-.978 1.492-.978 2.484 0 1.08.361 1.928 1.081 2.559.72.617 1.595.926 2.624.926zm-12.576-.167v-4.039c.193-.271.438-.412.759-.412.398 0 .592.27.592.81v3.641h2.714v-4.064c.167-.258.412-.387.733-.387.398 0 .592.27.592.81v3.641h2.714v-4.143c0-.874-.219-1.53-.669-1.98-.45-.45-.991-.682-1.647-.682-.887 0-1.736.463-2.096 1.068-.347-.707-.939-1.068-1.776-1.068-.823 0-1.607.502-1.916 1.03v-.863h-2.701v6.638h2.701zm-7.015-1.917c-.141.245-.386.373-.733.373-.412 0-.733-.244-.733-.655 0-.425.347-.644.798-.644.269 0 .488.077.668.232v.694zm2.958-.064c-.218 0-.333-.103-.333-.309v-1.774c0-.825-.309-1.48-.927-1.982-.617-.502-1.428-.759-2.431-.759-.965 0-1.814.142-2.534.438v2.019c.553-.283 1.196-.424 1.903-.424.888 0 1.325.334 1.325.99-.424-.244-.913-.361-1.492-.361-.656 0-1.248.181-1.775.529-.514.346-.772.874-.772 1.582 0 .63.219 1.157.643 1.582.425.411 1.004.617 1.737.617.888 0 1.621-.373 1.968-.874.219.476.939.874 1.724.874.566 0 1.016-.128 1.338-.398v-1.866c-.116.077-.232.116-.374.116zm-9.887 1.981v-3.743c.36-.386.9-.579 1.621-.579.27 0 .54.039.81.102v-2.547c-.09-.025-.218-.038-.36-.038-.463 0-.888.142-1.287.411-.398.271-.655.541-.784.811v-1.055h-2.701v6.638h2.701zm-5.352-4.528h1.826v-2.11h-1.826c0-.424.27-.643.798-.643.321 0 .669.052 1.028.168v-1.788c-.552-.194-1.17-.284-1.865-.284-1.762 0-2.675.965-2.675 2.368v.179H47v2.11h1.042v4.528h2.714v-4.528zm63.138-9.472v-4.039c.193-.27.436-.412.759-.412.399 0 .592.27.592.81v3.641h2.713V15.93c.167-.257.412-.386.734-.386.399 0 .592.27.592.81v3.641h2.713v-4.142c0-.875-.218-1.531-.668-1.981-.45-.45-.99-.682-1.646-.682-.888 0-1.737.463-2.097 1.068-.348-.708-.939-1.068-1.775-1.068-.824 0-1.608.502-1.917 1.029v-.862h-2.701v6.638h2.701zm-6.69-2.56c-.207.245-.463.36-.759.36-.373 0-.669-.257-.669-.81v-3.628h-2.714v4.207c0 1.634.771 2.598 2.2 2.598.411 0 .797-.103 1.158-.321.373-.219.63-.45.784-.695v.849h2.702v-6.638h-2.702v4.078zm-6.719-4.386c.862 0 1.518-.669 1.518-1.531 0-.424-.155-.772-.451-1.068-.295-.296-.643-.45-1.067-.45-.425 0-.785.154-1.081.45-.296.296-.437.644-.437 1.068 0 .424.141.797.437 1.093.296.296.656.438 1.081.438zm-1.365 6.946h2.715v-6.638H99.12v6.638zm-3.416 0v-3.743c.36-.386.9-.579 1.621-.579.27 0 .54.038.81.103v-2.547c-.09-.026-.22-.039-.36-.039-.463 0-.888.141-1.286.412-.399.27-.657.54-.785.81v-1.055h-2.701v6.638h2.701zm-8.367-4.065c.193-.322.527-.489 1.016-.489.656 0 1.094.554 1.094 1.235 0 .682-.438 1.235-1.094 1.235-.476 0-.823-.154-1.016-.476V15.93zm-2.676 4.065h2.714v-.746c.399.604 1.004.913 1.814.913.836 0 1.544-.334 2.097-.99.553-.656.835-1.492.835-2.496 0-1.003-.282-1.839-.835-2.495-.553-.656-1.261-.991-2.097-.991-.81 0-1.415.309-1.814.913v-3.112h-2.714v9.004zm-2.693-6.946c.861 0 1.518-.669 1.518-1.531 0-.424-.155-.772-.45-1.068-.296-.296-.643-.45-1.068-.45-.425 0-.785.154-1.081.45-.296.296-.436.644-.436 1.068 0 .424.14.797.436 1.093.296.296.656.438 1.081.438zm-1.363 6.946h2.714v-6.638h-2.714v6.638zm-2.43.167c.579 0 1.029-.09 1.377-.257v-2.071c-.09.038-.193.064-.321.064-.168 0-.348-.103-.348-.36v-6.547h-2.701v7.151c0 1.352.669 2.02 1.993 2.02zm-4.686-7.113c.861 0 1.517-.669 1.517-1.531 0-.424-.154-.772-.45-1.068-.296-.296-.643-.45-1.067-.45-.425 0-.785.154-1.081.45-.296.296-.437.644-.437 1.068 0 .424.141.797.437 1.093.296.296.656.438 1.081.438zm-1.364 6.946h2.714v-6.638h-2.714v6.638zm-4.027-2.56c-.206.245-.463.36-.759.36-.373 0-.669-.257-.669-.81v-3.628h-2.714v4.207c0 1.634.772 2.598 2.2 2.598.412 0 .798-.103 1.158-.321.373-.219.63-.45.784-.695v.849h2.701v-6.638h-2.701v4.078zm-7.993-.013c-.193.322-.528.489-1.017.489-.656 0-1.093-.553-1.093-1.235 0-.681.437-1.235 1.093-1.235.476 0 .824.155 1.017.476v1.505zm2.675-4.065h-2.714v.746c-.399-.604-1.004-.913-1.814-.913-.836 0-1.544.335-2.097.991-.553.656-.836 1.492-.836 2.495 0 1.004.283 1.84.836 2.496.553.656 1.261.99 2.097.99.81 0 1.415-.309 1.814-.913v3.318h2.714v-9.21zm-8.062 6.638v-2.38h-4.374v-1.261h3.371v-2.186h-3.371v-.798h4.297v-2.379h-7.062v9.004h7.139z" fill="#1792FF"/>
      <path fill="#FFF" d="M21.6587 2.957l-.026 5.016-4.211 2.457V5.414z"/>
      <path fill="#FFF" d="M17.4224 5.4141v5.016l-4.262-2.457.025-5.016z"/>
      <path fill="#1792FF" d="M21.6611 2.957l-4.237 2.457-4.237-2.457L17.3991.5z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M17.397.5l-4.211 2.457-.026 5.016 4.262 2.457 4.211-2.457.026-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M17.4243 10.4302v-5.016l-4.237-2.457M21.6587 2.957l-4.237 2.457"/>
      <path fill="#FFF" d="M25.9927 5.4385l-.025 4.991-4.237 2.482.025-5.016zM21.7544 7.8955l-.025 5.016-4.237-2.482v-4.991z"/>
      <path fill="#1792FF" d="M25.9912 5.4394l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M21.729 2.9824l-4.237 2.457v4.991l4.237 2.482 4.236-2.482.026-4.991z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M21.729 12.9116l.025-5.016-4.262-2.457M25.9907 5.4385l-4.237 2.457"/>
      <path fill="#FFF" d="M30.2544 7.9482l-.025 5.016-4.237 2.457.025-5.016z"/>
      <path fill="#FFF" d="M26.0181 10.4053l-.025 5.016-4.263-2.457.026-5.016z"/>
      <path fill="#1792FF" d="M30.2568 7.9482l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M25.9927 5.4912l-4.237 2.457-.025 5.016 4.262 2.457 4.237-2.457.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M25.9946 15.4214l.025-5.016-4.262-2.457M30.2524 7.9482l-4.237 2.457"/>
      <path fill="#FFF" d="M34.4985 10.4307l-.026 4.991-4.211 2.482v-5.016z"/>
      <path fill="#FFF" d="M30.2622 12.8877v5.016l-4.262-2.482v-4.991z"/>
      <path fill="#1792FF" d="M34.499 10.4307l-4.237 2.457L26 10.4307l4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M30.2368 7.9736l-4.237 2.457v4.991l4.262 2.482 4.212-2.482.025-4.991z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M30.2622 17.9038v-5.016l-4.262-2.457M34.4985 10.4307l-4.237 2.457"/>
      <path fill="#FFF" d="M17.4946 5.3379l-.025 5.016-4.212 2.483v-5.017z"/>
      <path fill="#FFF" d="M13.2583 7.8203v5.017l-4.262-2.483v-5.016z"/>
      <path fill="#1792FF" d="M17.4951 5.3379l-4.237 2.482-4.262-2.482 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.2329 2.8809l-4.237 2.457v5.016l4.262 2.483 4.212-2.483.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.2583 12.8369v-5.017l-4.262-2.482M17.4946 5.3379l-4.237 2.482"/>
      <path fill="#FFF" d="M13.3403 7.7422l-.025 4.991-4.237 2.482.025-5.016zM9.1021 10.1992l-.025 5.016-4.237-2.482v-4.991z"/>
      <path fill="#1792FF" d="M13.3389 7.7422l-4.237 2.457-4.262-2.457 4.237-2.482z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M9.0767 5.2598l-4.237 2.482v4.991l4.237 2.483 4.236-2.483.026-4.991z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M9.0767 15.2153l.025-5.016-4.262-2.457M13.3384 7.7422l-4.237 2.457"/>
      <path fill="#FFF" d="M8.9985 25.3008v5.017l-4.237 2.482v-5.017z"/>
      <path fill="#FFF" d="M4.7622 27.7832v5.017l-4.262-2.483.025-5.016zM9.0005 25.3008l-4.236 2.482-4.237-2.482 4.211-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M4.7368 22.8437l-4.211 2.457-.026 5.017 4.262 2.482 4.237-2.482v-5.017z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M4.7642 32.7998v-5.017l-4.237-2.482M8.9985 25.3008l-4.237 2.482"/>
      <path fill="#FFF" d="M13.3657 27.7051l-.025 4.99-4.212 2.484v-5.017zM9.1255 30.1621v5.017l-4.262-2.484.025-4.99z"/>
      <path fill="#1792FF" d="M13.3638 27.7051l-4.236 2.457-4.237-2.457 4.211-2.482z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M9.1001 25.2227l-4.211 2.482-.026 4.992 4.262 2.482 4.211-2.482.026-4.992z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M9.1274 35.1787v-5.017l-4.237-2.457M13.3657 27.7051l-4.237 2.457"/>
      <path fill="#FFF" d="M17.6724 15.2168l-.025 5.016-4.237 2.483.025-5.017z"/>
      <path fill="#FFF" d="M13.436 17.6992l-.025 5.017-4.263-2.483.026-5.016zM17.6748 15.2168l-4.237 2.482-4.262-2.482 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.4106 12.7598l-4.237 2.457-.025 5.016 4.262 2.483 4.237-2.483.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.4126 22.7158l.025-5.017-4.262-2.482M17.6704 15.2168l-4.237 2.482"/>
      <path fill="#FFF" d="M13.3403 12.8369l-.025 5.016-4.237 2.483.025-5.042zM9.1021 15.2939l-.025 5.042-4.237-2.483v-5.016z"/>
      <path fill="#FFF" d="M13.3389 12.8369l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M9.0767 10.3799l-4.237 2.457v5.016l4.237 2.483 4.236-2.483.026-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M9.0767 20.3359l.025-5.042-4.262-2.457M13.3384 12.8369l-4.237 2.457"/>
      <path fill="#FFF" d="M17.6724 15.3955l-.025 5.016-4.237 2.458.025-5.017z"/>
      <path fill="#FFF" d="M13.436 17.8525l-.025 5.017-4.263-2.458.026-5.016zM17.6748 15.3945l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.4106 12.9385l-4.237 2.457-.025 5.016 4.262 2.458 4.237-2.458.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.4126 22.8691l.025-5.017-4.262-2.457M17.6704 15.3955l-4.237 2.457"/>
      <path fill="#FFF" d="M17.6724 15.3955l-.025 5.016-4.237 2.458.025-5.017z"/>
      <path fill="#FFF" d="M13.436 17.8525l-.025 5.017-4.263-2.458.026-5.016zM17.6748 15.3945l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.4106 12.9385l-4.237 2.457-.025 5.016 4.262 2.458 4.237-2.458.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.4126 22.8691l.025-5.017-4.262-2.457M17.6704 15.3955l-4.237 2.457"/>
      <path fill="#FFF" d="M8.9985 20.2578v5.017l-4.237 2.457v-5.017z"/>
      <path fill="#FFF" d="M4.7622 22.7148v5.017l-4.262-2.457.025-5.017zM9.0005 20.2578l-4.236 2.457-4.237-2.457 4.211-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M4.7368 17.8008l-4.211 2.457-.026 5.017 4.262 2.457 4.237-2.457v-5.017z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M4.7642 27.7314v-5.017l-4.237-2.457M8.9985 20.2578l-4.237 2.457"/>
      <path fill="#FFF" d="M8.9985 15.2168v5.016l-4.237 2.483v-5.017z"/>
      <path fill="#FFF" d="M4.7622 17.6992v5.017l-4.262-2.483.025-5.016zM9.0005 15.2168l-4.236 2.482-4.237-2.482 4.211-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M4.7368 12.7598l-4.211 2.457-.026 5.016 4.262 2.483 4.237-2.483v-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M4.7642 22.7158v-5.017l-4.237-2.482M8.9985 15.2168l-4.237 2.482"/>
      <path fill="#FFF" d="M8.9985 10.2002v5.016l-4.237 2.457v-5.016z"/>
      <path fill="#FFF" d="M4.7622 12.6572v5.016l-4.262-2.457.025-5.016z"/>
      <path fill="#1792FF" d="M9.0005 10.2002l-4.236 2.457-4.237-2.457 4.211-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M4.7368 7.7441l-4.211 2.457-.026 5.016 4.262 2.457 4.237-2.457v-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M4.7642 17.6733v-5.016l-4.237-2.457M8.9985 10.2002l-4.237 2.457"/>
      <path fill="#FFF" d="M13.3403 12.7588l-.025 5.016-4.237 2.457.025-5.016zM9.1021 15.2158l-.025 5.016-4.237-2.457v-5.016z"/>
      <path fill="#1792FF" d="M13.3389 12.7588l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M9.0767 10.3018l-4.237 2.457v5.016l4.237 2.457 4.236-2.457.026-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M9.0767 20.2319l.025-5.016-4.262-2.457M13.3384 12.7588l-4.237 2.457"/>
      <path fill="#FFF" d="M17.6724 15.3193l-.025 5.016-4.237 2.458.025-5.017z"/>
      <path fill="#FFF" d="M13.436 17.7764l-.025 5.017-4.263-2.458.026-5.016z"/>
      <path fill="#1792FF" d="M17.6748 15.3184l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.4106 12.8623l-4.237 2.457-.025 5.016 4.262 2.458 4.237-2.458.025-5.016z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.4126 22.793l.025-5.017-4.262-2.457M17.6704 15.3193l-4.237 2.457"/>
      <path fill="#FFF" d="M17.6724 30.2656l-.025 5.017-4.237 2.457.025-5.017z"/>
      <path fill="#FFF" d="M13.436 32.7227l-.025 5.017-4.263-2.457.026-5.017z"/>
      <path fill="#1792FF" d="M17.6748 30.2646l-4.237 2.457-4.262-2.457 4.237-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M13.4106 27.8086l-4.237 2.457-.025 5.017 4.262 2.457 4.237-2.457.025-5.017z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M13.4126 37.7393l.025-5.017-4.262-2.457M17.6704 30.2656l-4.237 2.457"/>
      <path fill="#FFF" d="M21.9634 32.9014l-.026 5.017-4.211 2.457v-5.017z"/>
      <path fill="#FFF" d="M17.7271 35.3584v5.017l-4.262-2.457.025-5.017z"/>
      <path fill="#1792FF" d="M21.9653 32.9014l-4.236 2.457-4.237-2.457 4.211-2.457z"/>
      <path stroke="#1792FF" strokeLinejoin="round" d="M17.7017 30.4443l-4.211 2.457-.026 5.017 4.262 2.457 4.211-2.457.026-5.017z"/>
      <path stroke="#1792FF" strokeLinecap="round" strokeLinejoin="round" d="M17.729 40.375v-5.017l-4.237-2.457M21.9634 32.9014l-4.237 2.457"/>
    </g>
  </svg>
;

const CompareDeFiTable: React.FC = props => {
  const [ currentColumn, setCurrentColumn ] = useState(0);
  const { t } = useTranslation();

  return (
    <CompareDeFiWrap>
      <Jumbotron
        center={true}
        title={ t(`compareTable.Welcome_Future_Defi-title`) }
        subtitle={ t(`compareTable.Welcome_Future_Defi-subtitle`) }
      />
      <TableWrap role="table">
        <TableRow role="row" className="first">
          <TableColumnHeader role="rowheader"/>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>
            <img src="../maker-logo.png" alt="MAKER" style={{ width: '75%' }}/>
          </TableColumnSecondary>
          <TableColumnPrimary role="cell">
            <VigorLogo height={55} inverted={true} primary={true} horizontal={true} variant="special"/>
          </TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>
            <EquilibriumLogo/>
          </TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Stablecoin_Token`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>DAI</TableColumnSecondary>
          <TableColumnPrimary role="cell">VIGOR</TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>EOSDT</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Fee_Token`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>MKR</TableColumnSecondary>
          <TableColumnPrimary role="cell">VIG</TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>NUT</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Collateral_backed`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }><ShieldCheckSvgIcon/></TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }><ShieldCheckSvgIcon/></TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Collateral_Requirement`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>150%</TableColumnSecondary>
          <TableColumnPrimary role="cell">111%</TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>130%</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Borrow_Stablecoin`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }><ShieldCheckSvgIcon/></TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }><ShieldCheckSvgIcon/></TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Short_Selling`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>-</TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>-</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Bailout_Mechanism`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>{ t(`compareTable.Auction`) }</TableColumnSecondary>
          <TableColumnPrimary role="cell">{ t(`compareTable.Automatic`) }</TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>{ t(`compareTable.Auction`) }</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Onchain_Price_Discovery`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>-</TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>-</TableColumnSecondary>
        </TableRow>
        <TableRow role="row">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Risk_Modeling_Compliance`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>-</TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>-</TableColumnSecondary>
        </TableRow>
        <TableRow role="row" className="last">
          <TableColumnHeader role="rowheader">{ t(`compareTable.header.Stress_Test_Modeling`) }</TableColumnHeader>
          <TableColumnSecondary role="cell" className={ currentColumn === 1 ? 'hidden' : '' }>-</TableColumnSecondary>
          <TableColumnPrimary role="cell"><ShieldCheckSvgIcon/></TableColumnPrimary>
          <TableColumnSecondary role="cell" className={ currentColumn === 0 ? 'hidden' : '' }>-</TableColumnSecondary>
        </TableRow>
        <TableFooter>
          <div></div>
          <div></div>
          <div></div>
          <div>
            <PagingButton disabled={ currentColumn === 0} onClick={ () => setCurrentColumn(0) }/>
            <PagingButton disabled={ currentColumn === 1 } onClick={ () => setCurrentColumn(1) }/>
          </div>
        </TableFooter>
      </TableWrap>
    </CompareDeFiWrap>
  );
};

export default CompareDeFiTable;
