import React from 'react';
import styled from 'styled-components';
import { useTranslation, Trans } from 'react-i18next';
import JumbotronWithCard from './JumbotronWithCard';

import Borrow from '../assets/svgs/borrow.svg';
import Lend from '../assets/svgs/lend.svg';
import Hedge from '../assets/svgs/hedge.svg';

import { media } from "../utils/breakpoints";

const WhatCanYouDo: React.FC = props =>{

  const { t } = useTranslation();

  const title = ( <Trans i18nKey="whatCanYouDo.title"> What can you do with <span>VIGOR</span>?</Trans> );
  
  const theme = 'primary--color';
  
  const cards = [{
    title: t( `whatCanYouDo.borrow` ),
    logo: Borrow,
    description: t( `whatCanYouDo.borrowDescription` ),

  }, {
    title: t( `whatCanYouDo.lend` ),
    logo: Lend,
    description: t( `whatCanYouDo.lendDescription` ),
  },{
    title: t( `whatCanYouDo.hedge` ),
    logo: Hedge,
    description: t( `whatCanYouDo.hedgeDescription` ),
  }];
  
  const config = { theme, cards, title };


  return (
    <JumbotronWithCard {...config } />
  );
};

export default WhatCanYouDo;