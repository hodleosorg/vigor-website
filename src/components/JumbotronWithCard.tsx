import React from 'react';
import styled from 'styled-components';

import { media } from "../utils/breakpoints";

const JumbotronWrapper = styled.div`
  margin-top:100px;

  &primary--color span, &.primary--color h3 {
    color: ${props => props.theme.colors.primary};
  }

  &secondary--color span, &.secondary--color h3 {
    color: ${props => props.theme.colors.secondary};
  }
`;

const JumbotronSubtitle = styled.h2`
  font-size: 24px;
  font-weight: bold;
  max-width: 516px;
  letter-spacing: 0.75px;
  line-height: 29px;

  ${media.lessThan('xs-max')} {
    margin: 1rem;
  }
`;

const JumbotronSubtitle2 = styled.h3`
  font-size: 24px;
  color: ${props => props.theme.colors.secondary};
  font-weight: bold;
  max-width: 516px;
  letter-spacing: 0.75px;
  line-height: 29px;

`;

const JumbotronContentWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
`
const JumbotronRow= styled.div`
  flex-basis: 33%;


  p {
    width: 221px;
    font-size: 16px;
    line-height: 24px;
    font-weight: 400;
    letter-spacing: 0.3px;    
  }

  ${media.lessThan('md')} {
    flex-wrap: wrap;
    flex-direction: column;
    flex-basis: 44.5%;
    margin: 1rem auto;

    p {
      width: 288px; 
    }
  }

  ${media.lessThan('xs-max')} {
    flex-basis: 88.5%;
  }
`;

const Logo: React.FC<{ logo: any; height?: number }> = ({ logo, height }) => {
  if (typeof logo === `string`) {
    console.log('test passed');
    return <img height={height || 72} src={logo} />;
  }


  const Element = logo;
  return <Element height={height || 72} />;
};

type Props = {
  title: string,
  cards: [],
  theme: string
};


const JumbotronWithCard: React.FC<Props> = props =>{

  const { cards, title, theme } = props;

  const cardItems = cards.map( (card:any)=>(
    <React.Fragment>
      <JumbotronRow>
        <Logo logo={ card.logo } />
        <JumbotronSubtitle2>{ card.title }</JumbotronSubtitle2>
        <p>{ card.description }</p>
      </JumbotronRow>
    </React.Fragment>
  )); 

  return (
    <div>
    <JumbotronWrapper className={ theme }>
      <JumbotronSubtitle>
      { title }
      </JumbotronSubtitle>

      { cards && cards.length > 0 && 
        <JumbotronContentWrapper>

          { cardItems }

        </JumbotronContentWrapper>
          
      }
    </JumbotronWrapper></div>

  );
};

export default JumbotronWithCard;