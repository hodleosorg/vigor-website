import React from "react";
import Grid from "styled-components-grid";
import WelcomeToVigor from "../components/community/WelcomeToVigor";
import { GridCentered } from "../components/shared";
import GetInvolved from "../components/GetInvolved";
import HowToCandidate from "../components/community/HowToCandidate";
import CustodianBoard from "../components/community/CustodianBoard";

const Community: React.FC<{}> = props => {
  return (
    <React.Fragment>
      <GridCentered>
        <Grid.Unit size={{sm: 10 / 12}}>
          <WelcomeToVigor />
        </Grid.Unit>
      </GridCentered>

      <GridCentered>
        <Grid.Unit size={{sm: 10 / 12}}>
          <CustodianBoard />
        </Grid.Unit>
      </GridCentered>

      <GridCentered>
        <Grid.Unit size={{sm: 10 / 12}}>
          <HowToCandidate />
        </Grid.Unit>
      </GridCentered>

      <GridCentered>
        <Grid.Unit size={{sm: 10 / 12}}>
          <GetInvolved />
        </Grid.Unit>
      </GridCentered>
    </React.Fragment>
  );
};

export default Community;
